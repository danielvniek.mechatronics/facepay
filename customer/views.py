from django.forms.fields import ChoiceField
from django.shortcuts import render, redirect
from .forms import CustomerForm, UserForm, UserUpdateForm
from django.contrib import messages
from django.contrib.auth.models import Group
import business.models as Bmodels
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import ListView, DetailView, UpdateView, DeleteView

from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from users.custom_functions import is_business


from .models import Customer
# Create your views here.


def new(request):
    if request.method=='POST':
        bf = CustomerForm(request.POST, request.FILES)
        uf = UserForm(request.POST)
        if bf.is_valid() and uf.is_valid():
            uf.save()
            user = uf.instance
            bf.instance.user = user
            bf.save()
            c_group = Group.objects.get(name='Customers') 
            c_group.user_set.add(user)
            username = uf.cleaned_data.get('username')
            messages.success(request,f'FacePay account created for {username}! You are now able to log in.')
            return redirect('login')
    else:
        bf = CustomerForm()
        uf = UserForm()

    context = {
        'custom_form': bf,
        'user_form': uf, 
        'submit_text':'Subscribe business to FacePay'
    }
    return render(request, 'business/registration.html',context) 


@login_required
def MyProfile(request):
    if is_business(request.user):
        return redirect('business-update')
    else:
        Empl = Customer.getCustomer(request.user)
        return redirect('employee-update',pk = Empl.id)
    
class CustomerUpdate(LoginRequiredMixin,UserPassesTestMixin, UpdateView):
    model = Customer
    form_class = CustomerForm
    def get_success_url(self):
        return '/'
    def get_template_names(self):
        return "customer/update_form.html"
    def get_object(self):
        return(Customer.objects.filter(user = self.request.user).first())
        

    def form_valid(self,form):
        messages.success(self.request,f'{ self.get_object() }`s profile was successfully updated.')
        return super().form_valid(form)
    def test_func(self):
        if self.request.user.groups.filter(name='Customers').exists():
            return True

class CustomerDelete(LoginRequiredMixin,UserPassesTestMixin,DeleteView):
    model = Customer
    def test_func(self):
        if is_business(self.request.user):
            return False
        return True

    def get_object(self):
        return Customer.objects.filter(user = self.request.user).first()
        
    def get_success_url(self):
        return '/'
    def delete(self, request, *args, **kwargs):
        messages.success(self.request,f'{ self.get_object() }`s FacePay account was successfully deleted.')
        return super().delete(self, request, *args, **kwargs)

