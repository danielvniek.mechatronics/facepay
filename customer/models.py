from datetime import date
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User, Group
from django.urls import reverse
from PIL import Image
from django.contrib import admin
from django.core.validators import FileExtensionValidator
import os.path
from facepay import settings
from django.apps import apps
from payments import face_auth

class Customer(models.Model):
    #name, password and email captured in user

    
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField(null=True)
    creation_date = models.DateField(null=True)
    phone_number = models.CharField(max_length=12, blank = True, null = True)   
    bank = models.CharField(max_length=100)
    account_number = models.CharField(max_length=20)
    pin = models.CharField(max_length=4)
    account_type = models.CharField(max_length=100)
    image = models.ImageField(upload_to='customers')

    def name(self):
        return str(self.user.username)

    def age(self):
        return round((date.today()-self.birth_date).days/ 365.2425,2)
   
    def getEmployee(user):
        return Customer.objects.filter(user=user).first()
    
    def save(self, *args, **kwargs):
        try:
            oldSelf = Customer.objects.get(id=self.id)
            if oldSelf.image!=None and oldSelf.image!=self.image:
                oldSelf.image.delete(save=False)
            if oldSelf.contract!=None and oldSelf.contract!=self.contract:
                oldSelf.contract.delete(save=False)
        except: pass # when new p:
        if self.image == '':
            self.image = None

        super(Customer,self).save(*args, **kwargs)
        if self.image!=None:
            img = Image.open(self.image.path)
            img = img.crop(((img.width - min(img.size)) // 2,
                            (img.height - min(img.size)) // 2,
                            (img.width + min(img.size)) // 2,
                            (img.height + min(img.size)) // 2))
            if (img.height>600):
                output_size = (600,600)
                img.thumbnail(output_size)
            img.save(self.image.path)
            face_auth.face_register(self.image.path,self.name())

    def delete(self):
        self.image.delete(save=False)
        self.user.delete()
        super(Customer, self).delete()

    def __str__(self):
        return self.name()

    def get_absolute_url(self):
        return reverse('customer-detail', kwargs={'pk': self.pk})

admin.site.register(Customer)