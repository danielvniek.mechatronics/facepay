import django.forms as dforms
from django.contrib.auth.models import User, Group
import django.contrib.auth.forms as aforms
from django.forms.widgets import TextInput
from .models import Customer
import django.contrib.auth.forms as af
from django.contrib.auth.forms import UserCreationForm, UsernameField
class DateInput(dforms.DateInput):
    input_type = 'date'

class CustomerForm(dforms.ModelForm):  
    class Meta:
        model = Customer
        exclude= ('creation_date','user')#warnings will have own edit
        widgets = {'birth_date':DateInput()}
        help_texts = {'phone_number':'Use this format: +27831231234', 'image':'Please choose the best quality portrait image you have available of yourself. If the quality is too low, this might make payment processing slower when you shop'}
    BANK_CHOICES =(
    ("1", "FNB"),
    ("2", "ABSA"),
    ("3", "CAPITEC"),
    ("4", "NEDBANK"),
    ("5", "STANDARD BANK"),
    )
    bank = dforms.ChoiceField(choices=BANK_CHOICES)
    ACC_CHOICES =(
    ("1", "Check"),
    ("2", "Savings"),
    ("3", "Credit")
    )
    account_type = dforms.ChoiceField(choices=ACC_CHOICES)


class UserForm(aforms.UserCreationForm):  
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
    username = aforms.UsernameField(
        label='User Name',
        widget=dforms.TextInput(attrs={'autofocus': True})
    )

class UserUpdateForm(af.UserChangeForm):
    password = None
    class Meta:
        model = User
        fields = ['username', 'email']
    username = UsernameField(
        label='User Name',
        widget=TextInput(attrs={'autofocus': True})
    )
    