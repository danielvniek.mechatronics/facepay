from django.urls import path

from . import views
urlpatterns = [
    path('new/', views.new, name='customer-new'),
    path('update/', views.CustomerUpdate.as_view(), name='customer-update'),
    path('delete/', views.CustomerDelete.as_view(), name='customer-delete')
]