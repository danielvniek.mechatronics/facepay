from collections import defaultdict
import os
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from PIL import Image
from django.contrib import admin
from facepay import settings
from django.apps import apps

class Business(models.Model):
    #name, password and email captured in user
    user = models.OneToOneField(User, on_delete=models.CASCADE)  
    industry = models.CharField(max_length=100)    
    creation_date = models.DateField(default=timezone.now)
    bank = models.CharField(max_length=100)
    account_number = models.CharField(max_length=20)
    account_type = models.CharField(max_length=100)
    cart = models.JSONField(default=dict)
    cart_price = models.FloatField(default=0.0)
    logo = models.ImageField(null=True, blank = True,upload_to='businesses')
    message = models.TextField(null=True,blank=True)
   
    
    def pp(self):
        def_path = os.path.join(settings.MEDIA_ROOT, 'default_business.jpg')
        if self.logo==None or self.logo == '':
            TempBus = Business()
            TempBus.logo = def_path
            return TempBus.logo
        else:
            return self.logo

    def getBusiness(user):
        return Business.objects.filter(user=user).first()

    def save(self, *args, **kwargs):
        try:
            oldSelf = Business.objects.get(id=self.id)
            if oldSelf.logo!=None and oldSelf.logo!=self.image:
                oldSelf.logo.delete(save=False)
        except: pass # when new p:   
        if self.logo == '':
            self.logo = None
        super(Business,self).save(*args, **kwargs)
        if self.logo!=None:
            img = Image.open(self.logo.path)
            img = img.crop(((img.width - min(img.size)) // 2,
                            (img.height - min(img.size)) // 2,
                            (img.width + min(img.size)) // 2,
                            (img.height + min(img.size)) // 2))
            if (img.height>600):
                output_size = (600,600)
                img.thumbnail(output_size)
            img.save(self.logo.path)

    def delete(self): 
        self.logo.delete(save=False)
        self.user.delete()
        super(Business, self).delete()
        

    def __str__(self):
        return self.user.username

    def get_absolute_url(self):
        return reverse('business-detail', kwargs={'pk': self.pk})

admin.site.register(Business)