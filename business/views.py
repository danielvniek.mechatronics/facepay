from django.shortcuts import render, redirect


from .forms import BusinessForm, UserForm
from django.contrib import messages
from django.contrib.auth.models import Group
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import ListView, DetailView, UpdateView, DeleteView
from .models import Business
from django.db.models import Q
from users.custom_functions import is_business

def new(request):
    if request.method=='POST':
        bf = BusinessForm(request.POST, request.FILES)
        uf = UserForm(request.POST)
        if bf.is_valid() and uf.is_valid():
            uf.save()
            user = uf.instance
            bf.instance.user = user
            bf.save()
            business_group = Group.objects.get(name='Businesses') 
            business_group.user_set.add(user)
            username = uf.cleaned_data.get('username')
            messages.success(request,f'FacePay business account created for {username}! You are now able to log in.')
            return redirect('login')
    else:
        bf = BusinessForm()
        uf = UserForm()

    context = {
        'custom_form': bf,
        'user_form': uf, 
        'submit_text':'Subscribe business to FacePay'
    }
    return render(request, 'business/registration.html',context) 


class BusinessUpdate(LoginRequiredMixin,UserPassesTestMixin, UpdateView):
    model = Business
    form_class = BusinessForm
    def get_success_url(self):
        return '/'
    def get_template_names(self):
        return "customer/update_form.html"
    def get_object(self):
        return(Business.objects.filter(user = self.request.user).first())
        

    def form_valid(self,form):
        messages.success(self.request,f'{ self.get_object() }`s business profile was successfully updated.')
        return super().form_valid(form)
    def test_func(self):
        if self.request.user.groups.filter(name='Businesses').exists():
            return True

class BusinessDelete(LoginRequiredMixin,UserPassesTestMixin,DeleteView):
    model = Business
    def test_func(self):
        if is_business(self.request.user):
            return True
        return False

    def get_object(self):
        return Business.objects.filter(user = self.request.user).first()
        
    def get_success_url(self):
        return '/'
    def delete(self, request, *args, **kwargs):
        messages.success(self.request,f'{ self.get_object() }`s FacePay business account was successfully deleted.')
        return super().delete(self, request, *args, **kwargs)

