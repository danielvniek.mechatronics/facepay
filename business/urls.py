from django.urls import path

from . import views
urlpatterns = [
    path('new/', views.new, name='business-new'),
    path('update/', views.BusinessUpdate.as_view(), name='business-update'),
    path('delete/', views.BusinessDelete.as_view(), name='business-delete')
]