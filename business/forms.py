from django.forms import ModelForm, TextInput
import django.forms as dforms
from .models import Business
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UsernameField
import django.contrib.auth.forms as af

class BusinessForm(ModelForm):
    class Meta:
        model = Business
        fields = ['industry', 'bank','account_number','account_type','logo']
    BANK_CHOICES =(
    ("1", "FNB"),
    ("2", "ABSA"),
    ("3", "CAPITEC"),
    ("4", "NEDBANK"),
    ("5", "STANDARD BANK"),
    )
    bank = dforms.ChoiceField(choices=BANK_CHOICES)
    ACC_CHOICES =(
    ("1", "Check"),
    ("2", "Savings"),
    ("3", "Credit")
    )
    account_type = dforms.ChoiceField(choices=ACC_CHOICES)
    

class UserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
    username = UsernameField(
        label='Business Name',
        widget=TextInput(attrs={'autofocus': True})
    )

class UserUpdateForm(af.UserChangeForm):
    password = None
    username = None
    class Meta:
        model = User
        fields = ['email']
    
     
    
