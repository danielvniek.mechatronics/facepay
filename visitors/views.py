from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from users.custom_functions import is_business

# Create your views here.
def home(request):
    if request.user.is_authenticated:
        if is_business(request.user):
            return HttpResponseRedirect('products')
            
        else:
            return HttpResponseRedirect('payments/history')
    return render(request, 'visitors/home.html', {})



