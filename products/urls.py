from django.urls import path

from . import views

urlpatterns = [
     path('',views.shop,name='products-shop'),
     path('list/',views.ProductList.as_view(),name='product-list'),
     path('new/', views.new, name='products-new'),
     path('<int:pk>/update/', views.ProductUpdate.as_view(), name='product-update'),
     path('<int:pk>/delete/', views.ProductDelete.as_view(), name='product-delete')
]

