from django.forms import ModelForm
from .models import Product
import django.forms as dforms

class ProductForm(dforms.ModelForm):
    class Meta:
        model = Product
        exclude = ['user']##todo: exclude employee_tasks as well and have them in seperate list where they can be removed
