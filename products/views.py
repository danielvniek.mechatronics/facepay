from django.shortcuts import render, redirect
from .forms import ProductForm
from django.contrib import messages
from django.contrib.auth.models import Group
import business.models as Bmodels
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import ListView, DetailView, UpdateView, DeleteView
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from users.custom_functions import is_business


from .models import Product
# Create your views here.

@login_required
def new(request):##continue
    if request.method=='POST':
        ef = ProductForm(request.POST, request.FILES)
        if ef.is_valid():
            
            ef.instance.user = request.user 
            ef.save()
            messages.success(request,f"Successfully created new product")
            return redirect('product-list')
    else:
        ef = ProductForm()

    context = {
        'custom_form': ef,
        'submit_text': 'Create Product'
    }
    return render(request, 'business/registration.html',context) 

@login_required
def shop(request):
    if is_business(request.user):
        B = Bmodels.Business.objects.filter(user=request.user).first()
        if request.method=='POST':
            if 'Clear' in request.POST:
                cart={}
                cart_price=0
            elif 'FacePay' in request.POST:
                print("facepay")
                return redirect('/payments/new')##TODO
            
            elif 'Add' in request.POST:
                ID = request.POST.getlist('Add')[0]
                
                Prod = Product.objects.get(id=int(ID))
                cart = B.cart
                ExistingNr = cart.get(Prod.name,0)
                cart.update({Prod.name:ExistingNr+1})
                cart_price = B.cart_price + Prod.price     
                # do unsubscribe
            elif 'Remove' in request.POST:
                cart = B.cart
                ID = request.POST.getlist('Remove')[0]
                
                Prod = Product.objects.get(id=ID)
                ExistingNr = cart.get(Prod.name,0)
                if ExistingNr>0:
                    cart.update({Prod.name:ExistingNr-1})
                    cart_price = B.cart_price - Prod.price
                else:
                    cart_price = B.cart_price
        else:
            cart={}
            cart_price=0
        
        
        B.cart = cart
        B.cart_price = cart_price
        B.save()
        context = {
            'cart':cart,
            'cart_price':cart_price,
            'products': Product.objects.filter(user=request.user)
        }
        return render(request, 'products/shop.html',context) 
    else:
        raise PermissionDenied()

class ProductList(ListView, LoginRequiredMixin, UserPassesTestMixin):
    model = Product
    template_name = 'products/products.html'
    def test_func(self):
        if is_business(self.request.user) :
            return True
        return False
    def get_queryset(self):
        
        return Product.objects.filter(user=self.request.user)
    
              
class ProductDetail(LoginRequiredMixin,UserPassesTestMixin,DetailView):
    model = Product
    def test_func(self):
         Prod = self.get_object()
         if Prod.user==self.request.user:
                print("!!THIS Porud")
                return True
         return False

class ProductUpdate(LoginRequiredMixin,UserPassesTestMixin, UpdateView):
    model = Product
    fields = ["name","price","image"]
    def form_valid(self,F):
        messages.success(self.request,f"Your product named '{ self.get_object() }'' was successfully updated.")
        return super(ProductUpdate,self).form_valid(F)
    #def get_form(self, form_class=None):
      #  if form_class is None:
      #      form_class = self.get_form_class()
       # form = super(ProductUpdate, self).get_form(form_class)
        #form.fields['birth_date'].widget = DateInput()
       # return form
    def test_func(self):     
        Prod = self.get_object()
        self.success_url = '/products/list'
        ##permission
        if is_business(self.request.user):
            print("!!!!!!businESS")
            if Prod.user==self.request.user:
                print("!!THIS Porud")
                return True
        return False


class ProductDelete(LoginRequiredMixin,UserPassesTestMixin,DeleteView):
    model = Product
    def test_func(self):
        Prod = self.get_object()
        if is_business(self.request.user):
             if Prod.user==self.request.user:
                print("!!THIS Porud")
                return True
        return False
        
    def get_success_url(self):
        return '/products/list'
    def delete(self, request, *args, **kwargs):
        messages.success(self.request,f"Product named { self.get_object() } was successfully deleted.")
        return super().delete(self, request, *args, **kwargs)

