from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from PIL import Image
from django.apps import apps
from datetime import datetime
from datetime import date
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User, Group
from django.urls import reverse
from PIL import Image
from django.contrib import admin
from django.core.validators import FileExtensionValidator
import os.path
from facepay import settings
from django.apps import apps

class Product(models.Model):
    #name, password and email captured in user
    name = models.CharField(max_length=100)
    price = models.FloatField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)  ##set to do nothing so that def delete can be called
    image = models.ImageField(null = True, blank=True, upload_to='profile_pics')
    def pp(self):
        def_path = os.path.join(settings.MEDIA_ROOT, 'default_product.jpg')
        if self.image==None or self.image == '':
            TempEmpl = Product()
            TempEmpl.image = def_path
            return TempEmpl.image
        else:
            return self.image

    def lastMonthP(self):
        return "23.52 %" #TODO

    def todayN(self):
        return 5 #TODO
    
    def save(self, *args, **kwargs):
        try:
            oldSelf = Product.objects.get(id=self.id)
            if oldSelf.image!=None and oldSelf.image!=self.image:
                oldSelf.image.delete(save=False)
            if oldSelf.contract!=None and oldSelf.contract!=self.contract:
                oldSelf.contract.delete(save=False)
        except: pass # when new p:
        if self.image == '':
            self.image = None
        super(Product,self).save(*args, **kwargs)
        if self.image!=None:
            img = Image.open(self.image.path)
            img = img.crop(((img.width - min(img.size)) // 2,
                            (img.height - min(img.size)) // 2,
                            (img.width + min(img.size)) // 2,
                            (img.height + min(img.size)) // 2))
            if (img.height>600):
                output_size = (600,600)
                img.thumbnail(output_size)
            img.save(self.image.path)

    def delete(self): 
        self.image.delete(save=False)
        super(Product, self).delete()
        
        
        
        


    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('employee-detail', kwargs={'pk': self.pk})

admin.site.register(Product)