from django.contrib.auth.models import Group
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied

@login_required
def setup(request):
    if request.user.is_superuser:
        ##Create groups:
        new_group,created = Group.objects.get_or_create(name="Businesses")
        new_group,created = Group.objects.get_or_create(name="Customers")
        ##Create default activity type if it does not exist yet
        
        return redirect('home')
    else:
        return PermissionDenied()
