from django import forms
from django.contrib.auth.models import User
from django import forms 
from django.contrib.auth.forms import UserCreationForm, UsernameField



    
class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
    username = UsernameField(
        label='Business Name',
        widget=forms.TextInput(attrs={'autofocus': True})
    )

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']



