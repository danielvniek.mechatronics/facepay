from django.contrib.auth.models import User, Group

def is_business(user):
    return user.groups.filter(name="Businesses").exists()