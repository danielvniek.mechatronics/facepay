from django.urls import path

from . import views
urlpatterns = [
    path('new/', views.new, name='payments-new'),
    path('result/', views.result, name='payments-result'),
    path('history/', views.PaymentList.as_view(), name='payments-history')
]