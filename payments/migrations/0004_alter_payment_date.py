# Generated by Django 3.2.7 on 2021-10-17 08:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0003_auto_20211017_1038'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='date',
            field=models.DateTimeField(null=True),
        ),
    ]
