from datetime import date
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User, Group
from django.urls import reverse
from PIL import Image
from django.contrib import admin
from django.core.validators import FileExtensionValidator
import os.path
from facepay import settings
from django.apps import apps
from payments import face_auth

class Payment(models.Model):
    #name, password and email captured in user

    
    
    business = models.ForeignKey('business.Business',on_delete=models.DO_NOTHING)
    customer = models.ForeignKey('customer.Customer',on_delete=models.DO_NOTHING)
    date = models.DateTimeField(null=True)
    amount = models.FloatField(default=23.0)
    cart = models.JSONField(default=dict)

    def name(self):
        return str(self.customer)+"->"+str(self.business)+" on "+str(self.date)

    
   
    def __str__(self):
        return self.name()

    def get_absolute_url(self):
        return reverse('payment-detail', kwargs={'pk': self.pk})

admin.site.register(Payment)