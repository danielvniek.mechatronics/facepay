from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from django.core.exceptions import PermissionDenied
from facepay import settings
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import base64
import os
from business.models import Business
from customer.models import Customer
from . import face_auth
from django.contrib import messages
import json
from django.http import HttpResponseRedirect
from django.views.generic import ListView, DetailView, UpdateView, DeleteView
from .models import Payment
from datetime import datetime

from users.custom_functions import is_business
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

def result(request):
    B = Business.objects.filter(user=request.user).first()
    if B.message!=None:
        if 'payment' in B.message:
            MList = B.message.split(" ")
            CustName = MList[0]
            CustUser = User.objects.filter(username=CustName).first()
            Cust = Customer.objects.filter(user=CustUser).first()
            print(Cust)
            P = Payment()
            P.business = B
            P.customer = Cust
            P.cart = B.cart
            P.amount = B.cart_price
            P.date = datetime.now()
            P.save()
            messages.success(request,B.message)
            return redirect('/products/')
        else:
            messages.error(request,B.message)
            return redirect('/payments/new/')

@csrf_exempt
def new(request, *a, **kw):
    B = Business.objects.filter(user=request.user).first()
    if request.method=='POST':
        data = request.body.decode('ascii')
        dataO = json.loads(data)
        imgstring = dataO["img"]
        pin = dataO["pin"]
  
        list = imgstring.split(",")
        imgdata = base64.b64decode(list[1])
        
        filename = 'media\\checks\\'+str(request.user)+'_check.png'
        with open(filename, 'wb') as f:
            f.write(imgdata)
        
        match = face_auth.face_check(filename)
        if match!=None:
            print(match)
            U = User.objects.filter(username=match).first()
            C = Customer.objects.filter(user=U).first()
            if (pin==C.pin):
                print("Equal pins")
                B.message = f'{match} was successfully authenticated and the payment has been processed'
              
            else:
                print("not equal pins")
                B.message = 'ERROR: A FacePay customer account matched, but the pin is wrong'
           
        else:
            print("no match")
            B.message = 'ERROR: No FacePay customer account matched, please try again'
           
    else:
        B.message = ''
    B.save()
    return render(request, 'payments/new_payment.html',{}) 

class PaymentList(ListView, LoginRequiredMixin, UserPassesTestMixin):
    model = Payment
    template_name = 'payments/payments.html'
    def test_func(self):
        if is_business(self.request.user) :
            return True
        return False
    def get_queryset(self):
        if is_business(self.request.user) :
            B = Business.objects.filter(user=self.request.user).first()
            return Payment.objects.filter(business=B)
        else:
            C = Customer.objects.filter(user=self.request.user).first()
            return Payment.objects.filter(customer=C)


