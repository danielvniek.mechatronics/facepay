from ast import NameConstant
from types import new_class
import face_recognition
import os
import cv2
import json
import ast
import pickle

global faces
faces =[]
global names
names=[]

# os.chdir("D:\Documents\Hackathon\Scripts")

def appendToFile(data, fileName):
    with open(fileName, "a") as f:
        stringToWrite = ",".join(str(x) for x in data)
        f.write(f",{stringToWrite}")

def readStringFromFile(fileName):
    with open(fileName,"r") as f:
        string = f.read()
        stringArr = string.split(",")
        stringArr.remove("")
        return stringArr
       
def readFloatFromFile(fileName):
    with open(fileName,"r") as f:
        string = f.read()
        stringArr = string.split(",")
        stringArr.remove("")
        floatArr = list(map(float, stringArr))
        return floatArr


#face Registration- Returns a list of encodings (each index unique)
def face_register(path,name):
    global faces
    global names
    print('Registering User...')
    ##READ from file
    # faces_out= open("face_encodings.pickle",'a+b')  
    # names_out= open("names.pickle",'a+b')
   


   
    # names=pickle.load(names_out)
    # faces=pickle.load(faces_out)
   
    # names_out.close()
    # faces_out.close()

    # faces_out= open("face_encodings.pickle",'wb')  
    # names_out= open("names.pickle",'wb')

   
   
   
    image = face_recognition.load_image_file(path)
   
    encoding = face_recognition.face_encodings(image)
    if encoding:
        encoding = encoding[(0)]
        faces.append(encoding)
        names.append(name)

       
    else:
        print("Eish")
           
    #appendToFile(faces,'faces.txt')
    #appendToFile(names,'names.txt')

    # pickle.dump(faces,faces_out)
    # faces_out.close()
    print("This is names",names)
    # pickle.dump(names,names_out)
    # names_out.close()

    # names_in= open("names.pickle","rb")
    # known_names=pickle.load(names_in)
    # print("Test names: ",known_names)
    # names_in.close()
    #return True



#Face confirmation for payment
#Face confirmation for payment
def face_check(path):
    global names
    global faces
    ##create flipped version
    orig = cv2.imread(path)
    flippedI = cv2.flip(orig,1)
    list = path.split(".")
    flippedPath = list[0]+'_flipped.'+list[1]
    cv2.imwrite(flippedPath,flippedI)
    ##
    TOLERANCE=0.5
    print("Facial embedding verification in process...")

    # faces_in= open("face_encodings.pickle","rb")
    #faces_in= readFloatFromFile('faces.txt')
    known_faces=faces
    #known_faces=pickle.load(faces_in)
    #names_in= readStringFromFile('names.txt')
   
    known_names=names
    #print(known_names)
    MODEL = 'hog'  # default: 'hog', other one can be 'cnn' - CUDA accelerated (if available) deep-learning pretrained model

    print(known_faces)
    print(known_names)
    image = face_recognition.load_image_file(path)
    locations = face_recognition.face_locations(image, model=MODEL)
    encodings = face_recognition.face_encodings(image, locations)
    #image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    match = None
    for face_encoding, face_location in zip(encodings, locations):
        results = face_recognition.compare_faces(known_faces, face_encoding, TOLERANCE)
        if True in results:  # If at least one is true, get a name of first of found labels    
            match = known_names[results.index(True)]
            return match

    ### test flipped image
    image = face_recognition.load_image_file(flippedPath)
    locations = face_recognition.face_locations(image, model=MODEL)
    encodings = face_recognition.face_encodings(image, locations)
    #image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    match = None
    for face_encoding, face_location in zip(encodings, locations):
        results = face_recognition.compare_faces(known_faces, face_encoding, TOLERANCE)
        if True in results:  # If at least one is true, get a name of first of found labels    
            match = known_names[results.index(True)]
            return match

    return match